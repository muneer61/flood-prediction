#importing libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('main-flood-csv.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 9].values

# Splitting the dataset into the Training set and Test set
#from sklearn.cross_validation import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.10, random_state = 0)

testdataset=pd.read_csv('testcsv.csv')
X_test=testdataset.iloc[:,:-1].values
y_test=testdataset.iloc[:,9].values

# Fitting Logistic Regression to the Training set
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0)
classifier.fit(X, y)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

#confusin matrix for showing correct and incorrect 
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)




#accuray_test
from sklearn.metrics import accuracy_score
accuracy_score(y_test,y_pred)